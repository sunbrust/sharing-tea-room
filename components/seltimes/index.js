/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    changetime:{
      type:String,
      value:'hide',
      observer:'_onChangeTime'
    },
    notimes:{
      type:Object,
      value:[]
    },
    order:{
      type:Object,
      value:[]
    },
    mintime:{
      type:Number,
      value:2
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    status:'hide',
    current:0,
    d0:'02',
    d1:'',
    d2:'',
    nowdate:'',
    day1:'',
    day2:'',
    day3:'',
    view1:'',
    view2:'',
    view3:'',
    stime:'',
    etime:'',
    es:0
  },
  /**
   * 组件的方法列表
   */
  methods: {
    //当点击确定按钮时
    onSure: function () {
      if(this.data.es<this.data.mintime){
        wx.showToast({
          title: '当前房间为'+this.data.mintime+'小时起订',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      this.onCancel();
      this.triggerEvent('clicksure', {stime:this.data.stime,etime:this.data.etime,es:this.data.es})
    },
    seltimes:function(e){
      var time = e.currentTarget.dataset.val; //当前所选择的时间戳
      var stime = this.data.stime;
      var etime = this.data.etime;
      if(stime==''||(stime!=''&&time<stime)){
        this.setData({stime:time});
      }else if(stime==time){
        this.setData({stime:'',etime:''});
      }else{
        //检验时间是否重合
        var st = stime;
        var et = time;
        var outa = [];
        var outb = [];
        var D = ['00','01','02','03','04','05','06','07','08','09'];
        while(st<et){
          var w = new Date(st);
          var m = D[w.getMonth()+1]||w.getMonth()+1;//月
          var d = D[w.getDate()]||w.getDate();//日
          var Hours = D[w.getHours()]||w.getHours();
          var Minutes = D[w.getMinutes()]||w.getMinutes();
          var HM = Hours+':'+Minutes;
          var MDHM = m+'-'+d+' '+Hours+':'+Minutes;
          outa.push(HM);
          outb.push(MDHM);
          st = st+1800000;
        }
        var notimes = this.properties.notimes;
        var order = this.properties.order;
        var intersection1 = notimes.filter(function(v){ return outa.indexOf(v) > -1 });
        var intersection2 = order.filter(function(v){ return outb.indexOf(v) > -1 });
        if(intersection1.length==0&&intersection2.length==0){
          this.setData({etime:time});
        }else{
          wx.showToast({
            title: '灰色为不可选时间',
            icon: 'none',
            duration: 2000
          })
          return false;
        }
      }
      if(this.data.stime!=''&&this.data.etime!=''){
        var es = (this.data.etime-this.data.stime)/1000/3600;
      }else{
        var es = 0;
      }
      this.setData({es:es});
    },
    _onChangeTime:function(){
        var now = new Date();
        var t = now.getTime();
        var day1 = this.retuenTime(t);
        var view1 = this.returnView(t);
        var day2 = this.retuenTime(t+86400000);
        var view2 = this.returnView(t+86400000);
        var day3 = this.retuenTime(t+86400000+86400000);
        var view3 = this.returnView(t+86400000+86400000);
        this.setData({
          day1:day1,
          day2:day2,
          day3:day3,
          nowdate:day1.time,
          view1:view1,
          view2:view2,
          view3:view3
        });
        this.setData({
          status:'show',
          current:0,
          d0:'02',
          d1:'',
          d2:'',
          stime:'',
          etime:'',
          es:0
        });
    },
    onCancel:function(){
      this.setData({
        status:'hide'
      });
      this.properties.changetime = 'hide';
    },
    changeDate:function(e){
      this.setData({d0:'',d1:'',d2:''});
      var key = e.detail.current;
      switch(key){
          case 0:
            this.setData({d0:'02',current:0});
            break;
          case 1:
            this.setData({d1:'02',current:1});
            break;
          case 2:
            this.setData({d2:'02',current:2});
            break;
      }
    },
    onChangeDate:function(e){
      var key = e.currentTarget.id;
      switch(key){
          case 'd0':
            this.setData({d0:'02',current:0});
            break;
          case 'd1':
            this.setData({d1:'02',current:1});
            break;
          case 'd2':
            this.setData({d2:'02',current:2});
            break;
      }
    },
    onChaneDateTime:function(e){
      var date = e.detail.value.split("-");
      var now = new Date(date[0],date[1]-1,date[2]);
      var t = now.getTime();
      var day1 = this.retuenTime(t);
      var view1 = this.returnView(t);
      var day2 = this.retuenTime(t+86400000);
      var view2 = this.returnView(t+86400000);
      var day3 = this.retuenTime(t+86400000+86400000);
      var view3 = this.returnView(t+86400000+86400000);
      this.setData({
        day1:day1,
        day2:day2,
        day3:day3,
        view1:view1,
        view2:view2,
        view3:view3,
        d0:'02',
        current:0
      });
    },
    retuenTime:function(date){
      var now = new Date(date);
      var D = ['00','01','02','03','04','05','06','07','08','09'];
      var weeks = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");
      var w = weeks[now.getDay()]; //星期几
      var t = now.getTime(); //时间戳（毫秒）
      var y = now.getFullYear();//年
      var m = D[now.getMonth()+1]||now.getMonth()+1;//月
      var d = D[now.getDate()]||now.getDate();//日
      return {date:m+'月'+d+'日',week:w,time:y+'-'+m+'-'+d};
    },
    returnView:function(date){
        var n = new Date();
        var nowtime = n.getTime(); //当前时间戳
        var now = new Date(date);
        var newtime = new Date(now.getFullYear(),now.getMonth(),now.getDate(),0,0,0);
        var t = newtime.getTime(); //开始时间戳
        var ts = [];
        var i = 0;
        var D = ['00','01','02','03','04','05','06','07','08','09'];
        var notimes = this.properties.notimes;
        var order = this.properties.order;
        while(i<86400000){
          if(t+i>nowtime){
            var w = new Date(t+i);
            var m = D[w.getMonth()+1]||w.getMonth()+1;//月
            var d = D[w.getDate()]||w.getDate();//日
            var Hours = D[w.getHours()]||w.getHours();
            var Minutes = D[w.getMinutes()]||w.getMinutes();
            var HM = Hours+':'+Minutes;
            var MDHM = m+'-'+d+' '+Hours+':'+Minutes;
            var nt = notimes.indexOf(HM)>-1?false:true;
            var no = order.indexOf(MDHM)>-1?false:true;
            var type = nt&&no?0:1;
            ts.push({time:HM,s:t+i,type:type});
          }
          i = i+1800000;
        }
        return ts;
    }
  }
})
