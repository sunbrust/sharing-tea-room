/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
//获取应用实例
const app = getApp();
Page({
  data: {
    userInfo: {},
    phone:'',
    setting:[],
    kanum:10,
    isvip:0
  },
  onLoad: function () {
    var that = this;
    if (app.globalData.userInfo==undefined || JSON.stringify(app.globalData.userInfo)=='{}') {
      app.userInfoReadyCallback = res => {
        var phone = that.geTel(app.globalData.userInfo.phone);
        that.setData({
          userInfo:app.globalData.userInfo,
          phone:phone,
          isvip:app.globalData.userInfo.isvip
        });
      }
    }else{
      var phone = that.geTel(app.globalData.userInfo.phone);
      that.setData({
        userInfo:app.globalData.userInfo,
        phone:phone,
        isvip:app.globalData.userInfo.isvip
      });
    }
  },
  onShow:function(){
    wx.showShareMenu({
      withShareTicket:true,
      menus:['shareAppMessage','shareTimeline']
    })
  },
  bangding:function(e){
    if(e.detail.errMsg=="getPhoneNumber:ok"){
      console.log(e.detail);
      wx.showToast({
        title: '授权成功',
        icon: 'none',
        duration: 2000
      })
    }else{
      wx.showToast({
        title: '授权失败',
        icon: 'none',
        duration: 2000
      })
    }
  },
  kaimen:function(){
    wx.reLaunch({
      url: '/pages/code/index'
    })
  },
  dingdan:function(){
    wx.reLaunch({
      url: '/pages/order/index'
    })
  },
  gotouser:function(){
    wx.navigateTo({
      url: '/pages/vipuser/index'
    })
  },
  youhuiquan:function(){
    wx.navigateTo({
      url: '/pages/coupon/index'
    })
  },
  //消费记录
  xiaofei:function(){
    wx.navigateTo({
      url: '/pages/xiaofei/index'
    })
  },
   //续单
   xudan:function(){
    wx.navigateTo({
      url: '/pages/content/index?id=28&pid=7&oid=1'
    })
  },
  //优惠中心
  youhui:function(){
    wx.navigateTo({
      url: '/pages/buyhui/index'
    })
  },
  //充值记录
  chongzhi:function(){
    wx.navigateTo({
      url: '/pages/chongzhi/index'
    })
  },
  //包年记录
  baonian:function(){
    wx.navigateTo({
      url: '/pages/baonian/index'
    })
  },
  //加盟电话
  callphone:function(){
    wx.makePhoneCall({
      phoneNumber: app.globalData.setting.meng_tel
    })
  },
  geTel:function(tel){    
    var reg = /^(\d{3})\d{4}(\d{4})$/;
    return tel.replace(reg, "$1****$2");
  },
  onShareAppMessage: function (res) {
    return {
      title: '我在共享茶室等你',
      path: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  },
  onShareTimeline: function () {
    return {
      title: '我在共享茶室等你',
      query: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  }
})
