/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
const app = getApp();
var jishi;
Page({
  data: {
    option:[],
    userInfo: {},
    hasUserInfo: false,
    isloading:false,
    info:[],
    house:[],
    room:[],
    zkmoeny:0, //折扣的金额
    overtime:0,
    overtime_fen:'00',
    overtime_miao:'00',
    banka: {},
    chong: {},
    kajuan: {},
    vipset: {},
    isnianka:true,
    ischuzhi:true,
    kajuansel:-1, //卡卷选择
    czmoeny:0, //储值抵扣
    lastmoney:0,
    isdangqian:1,
    isunk:false
  },
  onHide: function(){
    this.setData({
      isdangqian:0
    });
  },
  onUnload: function () {
    this.setData({
      isdangqian:0
    });
  },
  onLoad: function (option) {
    clearInterval(jishi);
    wx.showLoading({
        title: '加载中',
    })
    var that = this;
    var option = option;
    if (app.globalData.userInfo==undefined || JSON.stringify(app.globalData.userInfo)=='{}') {
      app.userInfoReadyCallback = res => {
        that.setData({
          userInfo:app.globalData.userInfo,
          option:option
        });
        that.loadready(option);
      }
    }else{
      that.setData({
        userInfo:app.globalData.userInfo,
        option:option
      });
      that.loadready(option);
    }
  },
  onShow:function(){
    this.setData({
      isdangqian:1
    });
    wx.showShareMenu({
      withShareTicket:true,
      menus:['shareAppMessage','shareTimeline']
    })
  },
  //启动函数
  loadready:function(option){
    var that = this;
    var data = {
        "res": 1,
        "info": {
            "id": 1,
            "houseid": 7,
            "roomid": 28,
            "danjia": "100",
            "money": "200",
            "stime": "1617247800000",
            "etime": "1617255000000",
            "hours": "2",
            "number": "CS123456789",
            "ispay": 0,
            "type": 1,
            "date": "04月01日 11:30 - 04月01日 13:30"
        },
        "room": {
            "id": 28,
            "pid": 7,
            "name": "星辰大海",
            "ymoney": "200.00",
            "money": "100",
            "num": 18,
            "renshu": "1",
            "qiding": "2",
            "pic": "/image/con01.jpg"
        },
        "house": {
            "id": 7,
            "name": "支点科技店",
            "address": "支点科技",
            "yytime": "00:00 ~ 24:00",
            "lng": 110.196109,
            "lat": 20.045091,
            "desc": "欢迎光临",
            "tel": "4006660407"
        },
        "time": 120,
        "banka": false,
        "chong": {
            "id": 8,
            "hid": 7,
            "money": "2.00",
            "smoney": "0.04",
            "nmoney": 1.96
        },
        "kajuan": [{
          "id":1,
          "name":'我是现金抵用卷',
          "desc":'介绍一下吧~~~~',
          "date":'无限期',
          "money":50,
          "type":1
        },{
          "id":2,
          "name":'我是折扣卷',
          "desc":'介绍一下吧~~~~',
          "date":'无限期',
          "money":5,
          "type":2
        }],
        "vipset": {
            "id": 1,
            "money": "9.90",
            "zhekou": "9.5",
            "isshoudan": 0,
            "sdzhekou": "8.5",
            "isquan": 1,
            "quanmoney": "100.00"
        },
        "isunk": false
    };
    if(data.res==1){
      that.setData({
        info: data.info,
        room: data.room,
        house: data.house,
        banka: data.banka,
        chong: data.chong,
        kajuan: data.kajuan,
        kajuannum: data.kajuan.length,
        vipset: data.vipset,
        isloading:true,
        overtime:data.time,
        isunk:data.isunk
      }); 
      wx.hideLoading();
      that.daojishi();
      that.jiesuan();
    }else{
      wx.hideLoading();
      wx.showToast({
        title: data.msg,
        icon: 'none',
        duration: 2000,
        complete:function(){
          setTimeout(function () {
            wx.navigateBack();
          }, 2000)
        }
      })
    }
  },
  //倒计时
  daojishi:function(){
    var that = this;
    var D = ['00','01','02','03','04','05','06','07','08','09'];
    var times = that.data.overtime;
    if(times<=0){
      if(that.data.isdangqian==1){
        wx.showToast({
          title: '订单支付超时，请重新下单',
          icon: 'none',
          duration: 2000,
          complete:function(){
            wx.navigateBack({
              fail:function(){
                setTimeout(function () {
                  wx.reLaunch({
                    url: '/pages/map/index'
                  })
                }, 2000)
              }
            });
          }
        })
      }
    }else{
      var fen = Math.floor(times/60);
      var miao = times%60;
      var newtime = times-1;
      that.setData({
        overtime_fen:D[fen]||fen,
        overtime_miao:D[miao]||miao,
        overtime:newtime
      });
      jishi = setInterval(function () {
        clearInterval(jishi);
        that.daojishi();
      }, 1000);
    }
  },
  //支付前判定
  bepay:function(){
    var that = this;
    if(that.data.lastmoney>0){
      that.payment();
    }else{
      wx.showModal({
        title: '温馨提示',
        content: '是否支付该订单！',
        success (res) {
          if (res.confirm) {
            that.payment();
          }
        }
      })
    }
  },
  //支付
  payment:function(){
    var that = this;
    var res = {"res":1,"msg":'支付成功'}; //判定为 1 微信支付 2为0支付 0为错误
    if(res.res==0){
      wx.showToast({
        title: res.msg,
        icon: 'none',
        duration: 2000
      })
    }else if(res.res==2){
      wx.showToast({
        title: res.msg,
        icon: 'none',
        duration: 2000,
        complete:function(){
          setTimeout(function () {
            wx.reLaunch({
              url: '/pages/code/index'
            })
          }, 2000)
        }
      })
    }else{
        wx.navigateTo({
          url: '/pages/payment/index?id='+that.data.info.id
        })
    }
  },
  //包年服务开启
  baonian:function(e){
    this.setData({
      isnianka:e.detail.value
    });
    this.jiesuan();
  },
  //储值服务开启
  chuzhi:function(e){
    this.setData({
      ischuzhi:e.detail.value
    });
    this.jiesuan();
  },
  //卡卷选择
  selkajuan:function(){
    var that = this;
    wx.navigateTo({
      url: '/pages/couponsel/index',
      events: {
        someEvent: function(data) {
          that.setData({
            kajuansel:data.data
          });
          that.jiesuan();
        }
      },
      success: function(res) {
        res.eventChannel.emit('acceptDataFromOpenerPage', { data: that.data.kajuan })
      }
    })
  },
  //结算金额
  jiesuan:function(){
    var that = this;
    var shours = parseFloat(that.data.info.hours); //剩余时间
    var lastmoney = parseFloat(that.data.info.money); //剩余金额
    var zkmoeny = 0;
    var iskajuan = true;
    if(that.data.isnianka && that.data.banka){ //如果有年卡
      if(that.data.banka.hours<shours){ //如果年卡的时间没够用
        shours = shours-that.data.banka.hours;
      }else{
        iskajuan = false;
        shours = 0;
      }
      lastmoney = shours*that.data.info.danjia;
      if(lastmoney>0){ //如果金额大于0
        //VIP折扣
        if(that.data.userInfo.isvip=='1'){
          lastmoney = lastmoney*(that.data.vipset.zhekou/10)
        }
        //扣除优惠卷
        if(that.data.kajuan){ //如果有优惠卷
          if(that.data.kajuansel!='-1'){
            var youhuijuan = that.data.kajuan[that.data.kajuansel];
            if(youhuijuan['type']=='1'){ //金额抵消
              lastmoney = lastmoney-youhuijuan['money'];
            }else{ //折扣金额
              lastmoney = lastmoney*(youhuijuan['money']/10);
            }
          }
        }
      }
      zkmoeny = that.data.info.money-lastmoney;
      //储值使用 - 如果使用
      var czmoeny = 0;
      if(that.data.ischuzhi && that.data.chong){
        if(that.data.chong.nmoney>=lastmoney){
          czmoeny = lastmoney;
          lastmoney = 0;
        }else{
          czmoeny = that.data.chong.nmoney;
          lastmoney = lastmoney-that.data.chong.nmoney;
        }
      }
      that.setData({
        czmoeny:parseInt(czmoeny*100)/100,
        zkmoeny:parseInt(zkmoeny*100)/100,
        lastmoney:parseInt(lastmoney*100)/100,
        iskajuan:iskajuan
      });
    }else{ //如果没有年卡
      
        if(lastmoney>0){ //如果金额大于0
          //VIP折扣
          if(that.data.userInfo.isvip=='1'){
            lastmoney = lastmoney*(that.data.vipset.zhekou/10)
          }
          
          //扣除优惠卷
          if(that.data.kajuan){ //如果有优惠卷
            if(that.data.kajuansel!='-1'){
              var youhuijuan = that.data.kajuan[that.data.kajuansel];
              if(youhuijuan['type']=='1'){ //金额抵消
                lastmoney = lastmoney-youhuijuan['money'];
              }else{ //折扣金额
                lastmoney = lastmoney*(youhuijuan['money']/10);
              }
            }
          }
        }
        zkmoeny = that.data.info.money-lastmoney;
        //储值使用 - 如果使用
        var czmoeny = 0;
        
        if(that.data.ischuzhi && that.data.chong){
          if(that.data.chong.nmoney>=lastmoney){
            czmoeny = lastmoney;
            lastmoney = 0;
          }else{
            czmoeny = that.data.chong.nmoney;
            lastmoney = lastmoney-that.data.chong.nmoney;
          }
        }
        that.setData({
          czmoeny:parseInt(czmoeny*100)/100,
          zkmoeny:parseInt(zkmoeny*100)/100,
          lastmoney:parseInt(lastmoney*100)/100,
          iskajuan:iskajuan
        });
    }
  },
  //开通VIP链接
  ktvip:function(){
    wx.navigateTo({
      url: '/pages/vipuser/index'
    })
  },
  //进入优惠界面
  goyouhui:function(){
    wx.navigateTo({
      url: '/pages/buyhui/index'
    })
  },
  onShareAppMessage: function (res) {
    return {
      title: '我在共享茶室等你',
      path: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  },
  onShareTimeline: function () {
    return {
      title: '我在共享茶室等你',
      query: '/pages/map/index',
      imageUrl:'/image/share.jpg'
    }
  }
})
