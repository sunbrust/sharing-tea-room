/**
 * 长春市支点科技有限公司 [ http://www.4006660407.com ]
 * 小程序代码对外公开，以供大家进行参考和使用
 * 小程序中各项界面数据均已模拟，只需要简单的开发即可实际应用
 * 后台代码因版权原因无法公开，请谅解
 * 如需后端代码请咨询：400-666-0407
 * 或搜索微信公众号 zhidianweixin 或 支点Fulcrum
 * 共享茶室无缝对接互联网+时代，
 * 专业解决商务人士稳定而高频的商谈空间需求稀缺的痛点，
 * 符合各大企业外勤人员、小微企业创业 人员、自由职业者、异地差旅人士、小型会议、品茶爱好者等群体的需求。
 * 通过物联网技术实现无人值守的管理系统，
 * 开创全新“低门槛”、“高隐私”、“无营销”，
 * 24小时不打烊智能共享茶室模式。
 * 是您休闲会客、静心的好去处。
 * 已为多家茶馆提供专业管理系统解决方案。
 */
const app = getApp();
var jishi;
Page({
  data: {
    userInfo: {},
    isloading:true,
    info:{
      zhekou:'9.5',
      isshoudan:'1',
      isquan:'1',
      sdzhekou:'5',
      quanmoney:'50',
      money:'9.90'
    },
    paydata:[]
  },
  onLoad: function (option) {
    wx.showLoading({
        title: '加载中',
    })
    wx.hideLoading(); //隐藏加载
    var that = this;
    var option = option;
    if (app.globalData.userInfo==undefined || JSON.stringify(app.globalData.userInfo)=='{}') {
      app.userInfoReadyCallback = res => {
        that.setData({
          userInfo:app.globalData.userInfo
        });
      }
    }else{
      that.setData({
        userInfo:app.globalData.userInfo
      });
    }
  },
  //支付0
  payment:function(){
    wx.requestPayment({
      "timeStamp": '123123123',
      "nonceStr": '123123123',
      "package": '123123123',
      "signType": "MD5",
      "paySign": '123123123',
      "success":function(res){
        app.globalData.userInfo.isvip = 1;
        wx.reLaunch({
          url: '/pages/personal/index'
        })
      },
      "fail":function(res){
        wx.showToast({
          title: '支付失败',
          icon: 'none',
          duration: 2000,
          complete:function(){
            setTimeout(function () {
              wx.navigateBack();
            }, 2000)
          }
        })
        }
    })
  }
})
